create table user (
id integer primary key,
display_name VARCHAR(32) NOT NULL,
username VARCHAR(32) NOT NULL,
password VARCHAR(128) NOT NULL,
registered datetime DEFAULT (DATETIME('now', 'localtime')),
email VARCHAR(128) NOT NULL);

create table category (
id integer primary key,
title varchar(128) not null,
parentid integer not null);

create table thread (
id integer primary key,
catid integer,
title varchar(128) not null,
updated datetime DEFAULT (DATETIME('now', 'localtime')));

create table post (
id integer primary key,
threadid integer not null,
userid integer not null,
date datetime DEFAULT (DATETIME('now', 'localtime')),
body TEXT);

create table config (
name VARCHAR(32) NOT NULL,
value VARCHAR(128) NOT NULL);
INSERT INTO category(title, parentid) VALUES("General Discussion", 0);
INSERT INTO category(title, parentid) VALUES("Feedback", 0);
INSERT INTO config(name, value) VALUES('forum_name', 'The Discussion');
