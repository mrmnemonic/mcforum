/*
    mcForum
    Written 2014 by Martin A. COLEMAN.
    Dedicated to the Public Domain. See UNLICENSE for details.

SQL:
--------------------------------
create table user (
id integer primary key,
display_name VARCHAR(32) NOT NULL,
username VARCHAR(32) NOT NULL,
password VARCHAR(128) NOT NULL,
registered datetime DEFAULT (DATETIME('now', 'localtime')),
email VARCHAR(128) NOT NULL);

create table category (
id integer primary key,
title varchar(128) not null,
parentid integer not null);

create table thread (
id integer primary key,
catid integer,
title varchar(128) not null,
updated datetime DEFAULT (DATETIME('now', 'localtime')));

create table post (
id integer primary key,
threadid integer not null,
userid integer not null,
date datetime DEFAULT (DATETIME('now', 'localtime')),
body TEXT);

create table watchers (
threadid integer not null,
userid integer not null);

create table config (
name VARCHAR(32) NOT NULL,
value VARCHAR(128) NOT NULL);
INSERT INTO category(title, parentid) VALUES("General Discussion", 0);
INSERT INTO category(title, parentid) VALUES("Feedback", 0);
INSERT INTO config(name, value) VALUES('forum_name', 'The Discussion');
INSERT INTO config(name, value) VALUES('forum_theme', 'retro');
--------------------------------

Change Log
2014.01.06 - v0.1
- Initial version
- Uses qdecoder and sqlite3
- Basic support for categories, threads and posts.

2014.01.07 - v0.2
- Added better post support.
- Made category support a compile time option.
- Added structure to post and thread display.

2014.01.08 - v0.3
- Fixed post support.
- Made certain lines available in DEBUG mode only.
- Started moving config settings into database itself.
- Fixed new thread auto-view support.
- Using CSS to dress it up a bit.

2014.01.09 - v0.4
- User registration form and function.
- User login form and function.
- Preliminary check against multiple accounts.

2014.01.14 - v0.5
- Fixed user registration function.
- Fixed user login function.
- Added simple Xor function for basic password encryption against a cipher, but not using it yet.

2014.01.17 - v0.6
- XOR is broken for some reason. Found that out yesterday.
- Using MD5 for passwords now. It'll do. Will add SHA1 later.
- Fixed multi-line comments.

2014.01.18 - v0.7
- Fixed local time handling.
- Worked on single and double quotes handling.

2014.01.19 - v0.8
- Think I finally fixed quotes!
- Added basic profile support.

2014.01.20 - v0.85
- Added extra theme support tags.
- Added SMTP functions.
- Wrote README
- Wrote LICENSE
- Making smaller jumps with the versioning now.
- First public release.

2014.02.02 - v0.85a
- Ooops! It's 2014, not 2013. Corrected date in source and documentation.
- Changed license to 2-clause BSD.
- Now also available on github.

2014.03.01 - v0.86
- Added thread watchers table into database.
- Added support for watching threads.
- Tried to get SMTP support working.

2014.04.16 - v0.86a
- Officially renamed mcForum, due to another forum engine, written in PHP, named mforum.
- More input sanitising.

2014.04.16 - v0.87
- Fixed new member sign up bug (oops!)
- Finally implemented profile password changer.
- Attempted some cache denials with META tags. This should help improve caching smartphone access.

2014.05.04 - v0.87a
- Dedicated to the public domain.

2014.05.05 - v0.88
- Finally implemented the password changer.

2015.09.16 - v0.89
- More mobile friendly theme changes.
- Android forum viewer first implemented.
*/

/* adjust features here */
//#define SUPPORT_SMTP 1
//#define SUPPORT_CATEGORIES 1
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sqlite3.h>
#include "qdecoder.h"
#include "md5.c"

/* maybe move these to the database in a new table */
#define FORUM_NAME "The Discussion"
#define DB_FILE "forum.sq3"
#define VERSION "0.88"
#define THEME "retro"
/* #define DEBUG 1 */

/* forum configuration */
char site_url[]="index.cgi";
int opt_censor=0;

/* for SQLite3 */
sqlite3 *db;
char *sql=NULL;
char *zErrMsg=0;
sqlite3_stmt *stmt;
int sql_status=0;
int rc=0;

/* for qdecoder */
qentry_t *req;

/* for general use */
char *userid=NULL; /* track whether we are logged in or not */

void view_thread(char *);

void page_header()
{
    printf("<html>\n<head>\n<title>%s</title>\n \
<meta http-equiv=\"Cache-Control\" content=\"no-store\" />\n \
<meta http-equiv=\"Pragma\" content=\"no-cache\">\n \
<meta http-equiv=\"Expires\" content=\"-1\">\n \
<link href=\"%s.css\" rel=\"stylesheet\" type=\"text/css\" />\n \
</head>\n<body>\n \
<div id=\"title\"><a href=\"index.cgi\">%s</a></div><br>\n\n \
<center>\n", FORUM_NAME, THEME, FORUM_NAME);
    if(userid[0]=='0')
    {
        puts("<div id=\"actions\"><a href=\"index.cgi?cmd=signin\">Sign In</a> or <a href=\"index.cgi?cmd=register\">Register</a></div><br>");
    } else {
        puts("<div id=\"actions\">[<a href=\"index.cgi?cmd=profile\">View Profile</a>] - [<a href=\"index.cgi?cmd=logout\">Sign Out</a>]</div><br>");
    }
}

void page_footer()
{
    printf("\n</center>\n<div id=\"endnotice\">Using %s theme.<br>Powered by <a href=\"http://www.martincoleman.com/mcforum.html\">mcForum</a> v%s</div>\n</body>\n</html>\n", THEME, VERSION);
}

int find_results(char *query, int skip)
{
    /* for the results */
    char *page_title=NULL;
    char *page_desc=NULL;
    char *page_url=NULL;

    int num_results=0;

        /* sprintf(sql, "SELECT title, description WHERE title LIKE '%s' OR keywords LIKE '%s' OR description LIKE '%%%s", article, article, article, article, id); */
        sql=sqlite3_mprintf("SELECT COUNT(id) FROM sites WHERE title LIKE '%%%s%%' or keywords LIKE '%%%s%%' or description LIKE '%%%s%%'",query, query, query);
        if(sqlite3_prepare_v2(db, sql, strlen(sql), &stmt, NULL) == SQLITE_OK)
        {
            while( sqlite3_step(stmt) == SQLITE_ROW )
            {
                num_results = sqlite3_column_int(stmt, 0);
            }
            sqlite3_free(sql);
        }
         printf("Found %d matches for: %s<br><br>\n", num_results, query);
        sql=sqlite3_mprintf("SELECT url, title, description FROM sites WHERE title LIKE '%%%s%%' or keywords LIKE '%%%s%%' or description LIKE '%%%s%%' ORDER BY pop LIMIT %d, 10;",query, query, query, skip);
        #ifdef DEBUG
        printf("DEBUG: <B>%s</B><br>\n", sql);
        #endif
        rc=sqlite3_prepare_v2(db, sql, strlen(sql), &stmt, NULL);
        if(rc != SQLITE_OK)
        {
            printf("find_results: ERROR, database not ok!\n");
            sqlite3_close(db);
            return 0;
        }
        while(1)
        {
            sql_status = sqlite3_step(stmt);
            if(sql_status==SQLITE_ROW)
            {
                page_url = strdup((char *)sqlite3_column_text(stmt, 0));
                page_title = strdup((char *)sqlite3_column_text(stmt, 1));
                page_desc = strdup((char *)sqlite3_column_text(stmt, 2));
                printf("<b><a href=\"%s\">%s</a></b><br>%s<br>\n%s\n<br><br>\n", page_url, page_title, page_url, page_desc);
            } else if (sql_status == SQLITE_DONE) {
                break;
            } else {
                return 0;
            }
        }
        if(num_results>10 && skip>9)
        {
            printf("<a href=\"search.cgi?q=%s&skip=%d\"><&dash; Previous Page</a> | ", query, (skip-10));
        }
        if(num_results>10)
        {
            printf("<a href=\"search.cgi?q=%s&skip=%d\">Next Page &dash;></a>\n", query, (skip+10));
        }

    sqlite3_free(sql);
    sqlite3_finalize(stmt);
    return 1;
}

char *replace(const char *s, const char *old, const char *new)
{
    char *ret;
    int i, count = 0;
    size_t newlen = strlen(new);
    size_t oldlen = strlen(old);

    for (i = 0; s[i] != '\0'; i++)
    {
        if (strstr(&s[i], old) == &s[i])
        {
            count++;
            i += oldlen - 1;
        }
    }

    ret = malloc(i + count * (newlen - oldlen));
    if (ret == NULL)
        exit(EXIT_FAILURE);

    i = 0;
    while (*s)
    {
        if (strstr(s, old) == s)
        {
            strcpy(&ret[i], new);
            i += newlen;
            s += oldlen;
        } else
            ret[i++] = *s++;
    }
    ret[i] = '\0';

    return ret;
}

/* also consider http://en.wikipedia.org/wiki/Trimming_%28computer_programming%29#C.2FC.2B.2B */
char* rtrim(char* string, char junk)
{
    char* original = string + strlen(string);
    while(*--original == junk);
    *(original + 1) = '\0';
    return string;
}

char* ltrim(char *string, char junk)
{
    char* original = string;
    char *p = original;
    int trimmed = 0;
    do
    {
        if (*original != junk || trimmed)
        {
            trimmed = 1;
            *p++ = *original;
        }
    }
    while (*original++ != '\0');
    return string;
}

/* thread watch functions */
void add_watcher(char *threadid, char *userid)
{
    sql=sqlite3_mprintf("INSERT INTO watchers (threadid, userid) VALUES('%s', '%s')", threadid, userid);
    rc=sqlite3_exec(db, sql, NULL, 0, &zErrMsg);
    if(rc != SQLITE_OK)
    {
        printf("SQL Error: %s<br>", sqlite3_errmsg(db));
    }
    printf("<p>You have successfully subscribed!</p>\n");
    view_thread(threadid);
    sqlite3_free(sql);
}

void thread_alert(char *threadid, char *userid)
{
    char *email;
    char *title;
    char msg[128];

    msg[0]='\0';
    sql=sqlite3_mprintf("SELECT user.email FROM user WHERE user.id='%s'", userid);
    rc=sqlite3_prepare_v2(db, sql, -1, &stmt, NULL);
    rc=sqlite3_step(stmt);
    if (rc==SQLITE_ROW)
    {
        email = strdup((char *)sqlite3_column_text(stmt, 0));
    }
    rc=sqlite3_finalize(stmt);
    sprintf(msg, "A watched thread has been updated.\n%s?cmd=view&thread=%s\nDo not reply to this email.", site_url, threadid);
    #ifdef SUPPORT_SMTP
	send_mail("localhost", "noreply@mktechresearch.info", email, "A watched thread has been updated!", "noreply@mktechresearch.info", msg);
    #endif
    msg[0]='\0';
    sqlite3_free(sql);
}

void check_thread_watchers(char *threadid)
{
    char *watcherid;

    sql=sqlite3_mprintf("SELECT userid FROM watchers WHERE threadid='%s'", threadid);
    #ifdef DEBUG
    printf("DEBUG: <B>%s</B><br>\n", sql);
    #endif
    rc=sqlite3_prepare_v2(db, sql, -1, &stmt, NULL);
    if(rc != SQLITE_OK)
    {
        printf("check_thread_watchers: ERROR [%s]\n", sqlite3_errmsg(db));
        sqlite3_close(db);
        exit(0);
    }
    while(1)
    {
        sql_status = sqlite3_step(stmt);
        if(sql_status==SQLITE_ROW);
        {
            watcherid=strdup((char *)sqlite3_column_text(stmt, 0));
            //sprintf(threadid, "%d", thread);
            thread_alert(threadid, watcherid);
        }
    }
    sqlite3_finalize(stmt);
    sqlite3_free(sql);
}

void list_threads(char *catid)
{
    char *thread_id;
    char *thread_title;
    char *thread_created;

    sql=sqlite3_mprintf("SELECT id, title, updated FROM thread WHERE catid='%s' ORDER BY updated DESC;", catid);
    #ifdef DEBUG
    printf("DEBUG: <B>%s</B><br>\n", sql);
    #endif
    rc=sqlite3_prepare_v2(db, sql, strlen(sql), &stmt, NULL);
    if(rc != SQLITE_OK)
    {
        printf("list_threads: ERROR, database not ok! [%s]\n", sqlite3_errmsg(db));
        sqlite3_close(db);
        exit(0);
    }
    puts("<table id=\"wrapper\" border=1 cellspacing=1 cellpadding=2>");
    printf("<tr><th>TOPICS</th><th id=\"updated\">LAST UPDATED</th></tr>\n");
    while(1)
    {
        sql_status = sqlite3_step(stmt);
        if(sql_status==SQLITE_ROW)
        {
            thread_id = strdup((char *)sqlite3_column_text(stmt, 0));
            thread_title = strdup((char *)sqlite3_column_text(stmt, 1));
            thread_created = strdup((char *)sqlite3_column_text(stmt, 2));
            printf("<tr><td><a href=\"index.cgi?cmd=view&view=thread&thread=%s\">%s</a></td><td>%s</td></tr>\n", thread_id, thread_title, thread_created);
        } else if (sql_status == SQLITE_DONE) {
            break;
        } else {
            printf("SQL error");
            exit(0);
        }
    }
    puts("</table><br>");
    sqlite3_free(sql);
    sqlite3_finalize(stmt);
    if(strcmp(userid, "0")) printf("<p>Would you like to <a href=\"index.cgi?cmd=newthread&cat=%s\">start a new thread?</a></p><br>\n", catid);
}

#ifdef SUPPORT_CATEGORIES
void view_category(char *catid)
{
    char *cat_id;
    char *cat_title;

    sql=sqlite3_mprintf("SELECT id, title FROM category WHERE parentid='%s' ORDER BY title;", catid);
    #ifdef DEBUG
    printf("DEBUG: <B>%s</B><br>\n", sql);
    #endif
    rc=sqlite3_prepare_v2(db, sql, strlen(sql), &stmt, NULL);
    if(rc != SQLITE_OK)
    {
        printf("view_category: ERROR, database not ok!\n");
        sqlite3_close(db);
        exit(0);
    }
    while(1)
    {
        sql_status = sqlite3_step(stmt);
        if(sql_status==SQLITE_ROW)
        {
            cat_id = strdup((char *)sqlite3_column_text(stmt, 0));
            cat_title = strdup((char *)sqlite3_column_text(stmt, 1));
            printf("<b><a href=\"index.cgi?cmd=view&cat=%s\">%s</a></b><br>\n", cat_id, cat_title);
        } else if (sql_status == SQLITE_DONE) {
            break;
        } else {
            printf("SQL error");
            exit(0);
        }
    }
    if(atoi(catid)>0)
    {
        list_threads(catid);
    }
    sqlite3_free(sql);
    sqlite3_finalize(stmt);
}
#else
void view_category(char *catid)
{
    list_threads(catid);
}
#endif

void frm_newthread(char *catid)
{
    printf("<form action=\"index.cgi\" method=\"post\"> \
Title: <input type=\"text\" name=\"title\" size=30><br>\n \
<textarea name=\"content\" rows=5 cols=50></textarea><br>\n \
<input type=\"hidden\" name=\"cmd\" value=\"createthread\">\n \
<input type=\"hidden\" name=\"catid\" value=\"%s\">\n \
<input type=\"submit\" value=\"Create New Thread\">\n \
</form>\n", catid);
}

void frm_reply(char *thread)
{
    printf("<form action=\"index.cgi\" method=\"post\"> \
<textarea name=\"content\" rows=5 cols=50></textarea><br>\n \
<input type=\"hidden\" name=\"cmd\" value=\"addpost\">\n \
<input type=\"hidden\" name=\"thread\" value=\"%s\">\n \
<input type=\"submit\" value=\"Post Reply\">\n \
</form>\n", thread);
}

void create_thread(char *catid, char *title)
{
    sql=sqlite3_mprintf("INSERT INTO thread (catid, title) VALUES('%s', %Q);", catid, title);
    #ifdef DEBUG
    printf("DEBUG: <B>%s</B><br>\n", sql);
    #endif
    rc=sqlite3_exec(db, sql, NULL, 0, &zErrMsg);
    if(rc != SQLITE_OK)
    {
        printf("create_thread: ERROR, database not ok!\n");
        sqlite3_close(db);
        exit(0);
    }
}

void view_thread(char *threadid)
{
    char *post_userid;
    char *post_date;
    char *post_body;
    char *thread_title;

    printf("<a href=\"index.cgi\">&lt;-- Back to main</a><br>\n");
    #ifdef DEBUG
    printf("Viewing thread number %s\n", threadid);
    #endif

    sql=sqlite3_mprintf("SELECT title FROM thread WHERE id='%s'",threadid);
    if(sqlite3_prepare_v2(db, sql, strlen(sql), &stmt, NULL) == SQLITE_OK)
    {
        while( sqlite3_step(stmt) == SQLITE_ROW )
        {
            thread_title = strdup((char *)sqlite3_column_text(stmt, 0));
        }
        sqlite3_free(sql);
    }
    printf("<h2>Viewing: %s</h2>\n", thread_title);
    free(thread_title);

    sql=sqlite3_mprintf("SELECT post.userid, post.date, post.body, user.display_name FROM post, user WHERE user.id=post.userid AND post.threadid='%s' ORDER BY post.date;", threadid);
    #ifdef DEBUG
    printf("DEBUG: <B>%s</B><br>\n", sql);
    #endif
    rc=sqlite3_prepare_v2(db, sql, strlen(sql), &stmt, NULL);
    if(rc != SQLITE_OK)
    {
        printf("view_thread: ERROR, [%s]\n", sqlite3_errmsg(db));
        sqlite3_close(db);
        exit(0);
    }
    printf("<table id=\"wrapper\" border=0 cellspacing=1 cellpadding=2>\n");
    while(1)
    {
        sql_status = sqlite3_step(stmt);
        if(sql_status==SQLITE_ROW)
        {
            puts("<!-- response -->");
            post_userid = strdup((char *)sqlite3_column_text(stmt, 3));
            post_date = strdup((char *)sqlite3_column_text(stmt, 1));
            post_body = strdup((char *)sqlite3_column_text(stmt, 2));
            printf("<tr><td id=\"summary\" id=\"updated\" valign=top>Posted<br>By: %s<br>At: %s</td><td valign=top id=\"content\">%s</td></tr>\n", post_userid, post_date, replace(post_body, "\n", "<br>"));
            post_userid[0]='\0';
            post_date[0]='\0';
            post_body[0]='\0';
        } else if (sql_status == SQLITE_DONE) {
            break;
        } else {
            printf("SQL error");
            exit(0);
        }
    }
    printf("</table>\n<br>\n");
    sqlite3_free(sql);
    sqlite3_finalize(stmt);
    if(strcmp(userid, "0")) /* is the user logged in */
    {
        printf("<p>[<a href=\"index.cgi?cmd=reply&thread=%s\">Add Reply</a>][<a href=\"index.cgi?cmd=watch&thread=%s\">Watch This Thread</a>]</p><br>\n", threadid, threadid);
    }
}

void add_post(int threadid, char *userid, char *content)
{
    char *sane_content;
    
    sane_content=replace(content, "<", "&lt;");
    sane_content=replace(content, ">", "&gt;");

    if(opt_censor)
    {
    }

    /* insert post */
    sql=sqlite3_mprintf("INSERT INTO post (threadid, userid, body) VALUES('%d', '%s', %Q);", threadid, userid, content);
    /* sprintf(sql, "INSERT INTO post (threadid, userid, body) VALUES('%d', '%s', '%s');", threadid, userid, content); */
    #ifdef DEBUG
    printf("DEBUG: <B>%s</B><br>\n", sql);
    #endif
    rc=sqlite3_exec(db, sql, NULL, 0, &zErrMsg);
    if(rc != SQLITE_OK)
    {
        printf("Error: Cannot add_post [%s]\n", sqlite3_errmsg(db));
        sqlite3_close(db);
        exit(0);
    }
    /* sqlite3_free(sql); */
    
    /* update thread updated time */
    sql=sqlite3_mprintf("UPDATE thread SET updated=DATETIME('now', 'localtime') WHERE thread.id='%d';", threadid);
    #ifdef DEBUG
    printf("DEBUG: <B>%s</B><br>\n", sql);
    #endif
    rc=sqlite3_exec(db, sql, NULL, 0, &zErrMsg);
    if(rc != SQLITE_OK)
    {
        printf("Error: Cannot update_thread time [%s].\n", sqlite3_errmsg(db));
        sqlite3_close(db);
        exit(0);
    }
    /* sqlite3_free(sql); */
}

void frm_register(void)
{
    printf("<form action=\"index.cgi\" method=\"post\"> \
<b>Create New Account</b><br><br>\n \
Username: <input type=\"text\" name=\"username\"><br>\n \
Password: <input type=\"password\" name=\"password1\"><br>\n \
Password Confirm: <input type=\"password\" name=\"password2\"><br>\n \
Email: <input type=\"text\" name=\"email\"><br>\n \
<input type=\"hidden\" name=\"cmd\" value=\"createaccount\">\n \
<input type=\"submit\" value=\"Join\">\n \
</form>\n");
}

void frm_sign_in()
{
    printf("<form action=\"index.cgi\" method=\"post\"> \
<!-- <b>Sign In</b><br><br>\n--> \
Username: <input type=\"text\" name=\"username\"><br>\n \
Password: <input type=\"password\" name=\"password\"><br>\n \
<input type=\"hidden\" name=\"cmd\" value=\"login\">\n \
<input type=\"submit\" value=\"Login\">\n \
</form>\n");
}

int get_user_id(char *username, char *password)
{
    int results=0;
    char real_password[32];
    md5_encrypt(password, real_password);
    sql=sqlite3_mprintf("SELECT id FROM user WHERE username='%s' AND password='%s'", username, real_password);
    rc=sqlite3_prepare_v2(db, sql, -1, &stmt, NULL);
    rc=sqlite3_step(stmt);
    if (rc==SQLITE_ROW)
        results = sqlite3_column_int(stmt, 0);
    rc=sqlite3_finalize(stmt);
    return results;
}

int get_user_id1(char *username, char *password)
{
    int results=0;
    char real_password[32];
    md5_encrypt(password, real_password);
    sql=sqlite3_mprintf("SELECT id FROM user WHERE username='%s' AND password='%s'", username, real_password);
    /* sprintf(sql, "SELECT id FROM user WHERE username='%s' AND password='%s'", username, password); */
    #ifdef DEBUG
    printf("<b>%s</b>\n", sql);
    #endif
    if(sqlite3_prepare_v2(db, sql, -1, &stmt, NULL) != SQLITE_OK)
    {
        printf("SQL Error: %s<br>", sqlite3_errmsg(db));
        return results;
    }
    while( sqlite3_step(stmt) == SQLITE_ROW )
    {
        results = sqlite3_column_int(stmt, 0);
        printf("user_id: %d", results);
    }
    sqlite3_free(sql);
    #ifdef DEBUG
    printf("SR: %d<br>", results);
    #endif
    sqlite3_finalize(stmt);
    return results;
    /* TRY 2 */
    /*
    if(sqlite3_prepare_v2(db, sql, strlen(sql), &stmt, NULL) == SQLITE_OK)
    {
        results=sqlite3_column_count(stmt);
    }
    return results;
    */
    /* TRY 1 */
    /*
    int real_userid=0;
    sql=sqlite3_mprintf("SELECT id FROM user WHERE username='%s' AND password='%s'", username, password);
    if(sqlite3_prepare_v2(db, sql, strlen(sql), &stmt, NULL) == SQLITE_OK)
    {
        while( sqlite3_step(stmt) == SQLITE_ROW )
        {
            real_userid = sqlite3_column_int(stmt, 0);
        }
        sqlite3_free(sql);
    }
    return real_userid;
    */
}

void create_account(char *username, char *password, char *email)
{
    char real_password[32];
    if(!get_user_id(username, password))
    {
        md5_encrypt(password, real_password);
        sql=sqlite3_mprintf("INSERT INTO user (display_name, username, password, email) VALUES('%s', '%s', %Q, '%s')", username, username, real_password, email);
        #ifdef DEBUG
        printf("<b>%s</b><br>\n", sql);
        #endif
        rc=sqlite3_exec(db, sql, NULL, 0, &zErrMsg);
        if(rc != SQLITE_OK)
        {
            printf("Error: Cannot create new account for some reason [%s].\n", sqlite3_errmsg(db));
            sqlite3_close(db);
            exit(0);
        }
        printf("<font color=\"#007700\"><b>New account created! You may now sign in.</b></font><br>\n");
    } else {
        qcgires_error(req, "That username already exists");
    }
}

void view_profile(char *myuserid)
{
    char *display_name;
    char *email;
    char *registered;

    sql=sqlite3_mprintf("SELECT display_name, email, registered FROM user WHERE id='%q'", myuserid);
    rc=sqlite3_prepare_v2(db, sql, -1, &stmt, NULL);
    rc=sqlite3_step(stmt);
    if (rc==SQLITE_ROW)
    {
        display_name = strdup((char *)sqlite3_column_text(stmt, 0));
        email = strdup((char *)sqlite3_column_text(stmt, 1));
        registered = strdup((char *)sqlite3_column_text(stmt, 2));
    }
    rc=sqlite3_finalize(stmt);
    printf("<form action=\"index.cgi\" method=\"post\">\n \
<table width=\"500\" align=center>\n \
<tr><td colspan=2 align=center>%s's Profile</td></tr>\n \
<tr><td>Display Name:</td><td><input type=\"text\" name=\"display_name\" value=\"%s\" maxlength=\"32\"></td></tr>\n \
<tr><td>Email:</td><td><input type=\"text\" name=\"email\" value=\"%s\"></td></tr>\n \
<tr><td>Password:</td><td><input type=\"password\" name=\"new_password1\"></td></tr>\n \
<tr><td>Password again:</td><td><input type=\"password\" name=\"new_password2\"></td></tr>\n \
<tr><td>Registered:</td><td>%s</td></tr>\n \
<tr><td colspan=2 align=center><input type=\"hidden\" name=\"cmd\" value=\"update_profile\"></td></tr>\n \
<tr><td colspan=2 align=center><input type=\"submit\" value=\"Update\"></td></tr>\n \
</table>\n \
</form><br>\n* Not supported yet.<br>\n", display_name, display_name, email, registered);
}

void update_profile(char *myuserid, char *email, char *display_name, char *new_password)
{
    char *cleaned_password;
    char enc_password[32];

    cleaned_password=ltrim(rtrim(new_password, ' '), ' ');
    #ifdef DEBUG
    printf("DEBUG TEST: cleaned_password [%s]<br>\n", cleaned_password);
    #endif
    if(strlen(cleaned_password)>1)
    {
        md5_encrypt(cleaned_password, enc_password);
        sql=sqlite3_mprintf("UPDATE user SET email=%Q, display_name=%Q, password='%s' WHERE id='%s'", email, display_name, enc_password, myuserid);
    } else {
        sql=sqlite3_mprintf("UPDATE user SET email=%Q, display_name=%Q WHERE id='%s'", email, display_name, myuserid);
    }
    rc=sqlite3_exec(db, sql, NULL, 0, &zErrMsg);
    if(rc != SQLITE_OK)
    {
        printf("SQL Error: %s<br>", sqlite3_errmsg(db));
    }
    #ifdef DEBUG
    printf("DEBUG TEST: %s<br>\n", sql);
    #endif
    printf("Done! You may now visit the <a href=\"index.cgi\">main page</a>\n");
}

int main(void)
{
    /* for searching */
    char *cmd;
    char *view;
    char *category;
    char *username=NULL;
    char *password=NULL;
    char *new_password1=NULL;
    char *new_password2=NULL;
    char *query=NULL;
    char *title=NULL;
    char *content=NULL;
    char *catid=NULL;
    char *thread=NULL;
    int new_threadid=0;
    char *password1;
    char *password2;
    char *email;
    char *display_name;
    char tmp[128];
    tmp[0]='\0';
    int exists=0;

    /* init qdecoder */
    req = qcgireq_parse(NULL, 0);

    /* get our command */
    cmd = (char *)req->getstr(req, "cmd", false);
    if (cmd == NULL) cmd = "view";
    if (strlen(cmd)<1) cmd = "view";

    view = (char *)req->getstr(req, "view", false);
    if (view == NULL) view = "category";
    if (strlen(view)<1) cmd = "category";

    /*
    query = (char *)req->getstr(req, "query", false);
    if (query == NULL) query = "view";
    if (strlen(query)<1) query = "view";
    */

    thread = (char *)req->getstr(req, "thread", false);

    #ifdef SUPPORT_CATEGORIES
    category = (char *)req->getstr(req, "cat", false);
    if (category == NULL) category = "0";
    if (strlen(category)<1) category = "0";
    #else
    category="0";
    #endif

    /* open our database */
    rc=sqlite3_open(DB_FILE, &db);
    if(rc)
	{
        printf("ERROR, cannot open database.\n");
        page_footer();
		sqlite3_close(db);
        req->free(req);
		return 0;
	}

    /* are we trying to log in? */
    if(!strcmp(cmd, "login"))
    {
        username = (char *)req->getstr(req, "username", false);
        password = (char *)req->getstr(req, "password", false);
        exists=get_user_id(username, password);
        if(!exists)
        {
            qcgires_error(req, "Incorrect username or password");
        } else {
            qcgires_setcookie(req, "username", username, 0, NULL, NULL, false);
            sprintf(tmp, "%d", exists);
            qcgires_setcookie(req, "userid", tmp, 0, NULL, NULL, false);
            printf("<p>Successfully signed in. You may visit the <a href=\"index.cgi\">main index</a>.</p>\n");
        }
    } else {
        userid = (char *)req->getstr(req, "userid", false);
    }
    if (userid == NULL) userid = "0";
    if (strlen(userid)<1) userid = "0";

    if(!strcmp(cmd, "logout"))
    {
        qcgires_removecookie(req, "username", NULL, NULL, false);
        qcgires_removecookie(req, "userid", NULL, NULL, false);
    }

    /* prepare for rendering */
    qcgires_setcontenttype(req, "text/html");
    page_header();

    if(!strcmp(cmd, "login"))
    {
        printf("<p>Successfully signed in. You may visit the <a href=\"index.cgi\">main index</a>.</p>\n");
    }
    if(!strcmp(cmd, "logout"))
    {
        printf("<p>Successfully signed out. You may visit the <a href=\"index.cgi\">main index</a> again if you like.</p>\n");
    }

    /* let's see what we need to do */
    if(!strcmp(cmd, "view"))
    {
        if(!strcmp(view, "thread"))
        {
            view_thread(thread);
        }
        if(!strcmp(view, "category"))
        {
            view_category(category);
        }
    }
    if(!strcmp(cmd, "reply"))
    {
        frm_reply(thread);
    }
    if(!strcmp(cmd, "addpost"))
    {
        content = (char *)req->getstr(req, "content", false);
        
        add_post(atoi(thread), userid, content);
        view_thread(thread);
        //check_thread_watchers(thread);
    }
    if(!strcmp(cmd, "profile"))
    {
        view_profile(userid);
    }
    if(!strcmp(cmd, "update_profile"))
    {
        display_name = (char *)req->getstr(req, "display_name", false);
        email = (char *)req->getstr(req, "email", false);
        new_password1 = (char *)req->getstr(req, "new_password1", false);
        new_password2 = (char *)req->getstr(req, "new_password2", false);
        if(strcmp(new_password1, new_password2))
        {
            update_profile(userid, email, display_name, new_password1); /* not changing password */
        } else {
            update_profile(userid, email, display_name, new_password2); /* updating password */
        }
    }
    /* perform search. taken from Front Desk */
    if(!strcmp(cmd, "search"))
    {
        if(!find_results(query, 10)) /* FIXME */
        {
            printf("Error retrieving results.\n");
        }
    }
    if(!strcmp(cmd, "newthread"))
    {
        frm_newthread(category);
    }
    if(!strcmp(cmd, "register"))
    {
        frm_register();
    }
    if(!strcmp(cmd, "signin"))
    {
        frm_sign_in();
    }
    if(!strcmp(cmd, "createaccount"))
    {
        username = (char *)req->getstr(req, "username", false);
        password1 = (char *)req->getstr(req, "password1", false);
        password2 = (char *)req->getstr(req, "password2", false);
        email = (char *)req->getstr(req, "email", false);
        if(strlen(password1)<8)
        {
            qcgires_error(req, "Password should be a minimum of 8 characters.");
        }
        #ifdef DEBUG
        printf("U: %s P1: %s P2: %s E: %s<br>\n", username, password1, password2, email);
        #endif
        if(strcmp(password1, password2))
        {
            qcgires_error(req, "Passwords do not match. Please try again.");
        } else {
            create_account(username, password1, email);
        }
    }
    if(!strcmp(cmd, "createthread"))
    {
        content = (char *)req->getstr(req, "content", false);
        title = (char *)req->getstr(req, "title", false);
        catid= (char *)req->getstr(req, "catid", false);
        if(title==NULL || content==NULL)
        {
            qcgires_error(req, "Invalid thread data");
        }
        if(strlen(title)==0)
        {
            qcgires_error(req, "Invalid title");
        }
        if(strlen(content)==0)
        {
            qcgires_error(req, "Invalid content");
        }
        create_thread(catid, title);
        new_threadid=sqlite3_last_insert_rowid(db);
        add_post(new_threadid, userid, content);
        /* snprintf(thread, 10, "%d", new_threadid); */
        sprintf(tmp, "%d", new_threadid);
        #ifdef DEBUG
        printf("DEBUG: new thread id: [%d] and string: [%s]\n", new_threadid, tmp);
        #endif
        view_thread(tmp);
    }
    if(!strcmp(cmd, "watch"))
    {
        add_watcher(thread, userid);
    }
    page_footer();

    /* free up everything */
	sqlite3_close(db);
    req->free(req);
    return 0;
}
